package com.lsat.corefac.model;

import javax.persistence.*;

import com.lsat.corefac.util.UtilEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;


@Entity
@Table(name = "tdocument")
@Getter
@Setter
public class Document implements  Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long cdocument;

    @Column(name = "ccompany", nullable = false)
    private Long ccompany;


    @Column(name="name",length = 120)
    private String name;

    @Column(name="keyAcces",length = 120)
    private String keyAcces;
    @Column(name="alias_doc",length = 10, nullable = false)
    private String aliasDoc;

    private Character state;

    @Temporal(TIMESTAMP)
    private Date dateRegistre;

    @Temporal(TIMESTAMP)
    private Date dateFinalize;

    @Enumerated(EnumType.ORDINAL)
    private UtilEnum.PASO paso;


    @Transient
    private Boolean error = false;


    @Transient
    private String message;


}
