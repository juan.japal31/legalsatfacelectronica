package com.lsat.corefac.model;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;
@Entity
@Table(name = "txml")
@Getter
@Setter
public class Xml  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long cxml;

    private Long cdocument;
    @Column(name = "xml_normal", columnDefinition = "TEXT")
    protected String xmlNormal;

    @Column(name = "xml_signed", columnDefinition = "TEXT")
    protected String xmlSigned;

    @Column(name = "xml_authorized", columnDefinition = "TEXT")
    protected String xmlAuthorized;

}
