package com.lsat.corefac.model;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "tstep")
@Getter
@Setter
public class Step implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long cstep;

    private Long cdocument;

    @Column(name = "num_step")
    protected String NumStep;

    @Column(name = "code_error", length = 10)
    protected String codeError;

    @Column(name = "mesager", length = 125)
    protected String mesager;

    @Column(name = "description", length = 1255)
    protected String description;

}
