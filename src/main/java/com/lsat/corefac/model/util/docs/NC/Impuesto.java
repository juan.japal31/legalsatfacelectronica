/*     */ package com.lsat.corefac.model.util.docs.NC;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="impuesto", propOrder={"codigo", "codigoPorcentaje", "tarifa", "baseImponible", "valor"})
/*     */ public class Impuesto
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String codigo;
/*     */   @XmlElement(required=true)
/*     */   protected String codigoPorcentaje;
/*     */   protected BigDecimal tarifa;
/*     */   @XmlElement(required=true)
/*     */   protected BigDecimal baseImponible;
/*     */   @XmlElement(required=true)
/*     */   protected BigDecimal valor;
/*     */   
/*     */   public String getCodigo()
/*     */   {
/*  62 */     return this.codigo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigo(String value)
/*     */   {
/*  74 */     this.codigo = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCodigoPorcentaje()
/*     */   {
/*  86 */     return this.codigoPorcentaje;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigoPorcentaje(String value)
/*     */   {
/*  98 */     this.codigoPorcentaje = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getTarifa()
/*     */   {
/* 110 */     return this.tarifa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTarifa(BigDecimal value)
/*     */   {
/* 122 */     this.tarifa = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getBaseImponible()
/*     */   {
/* 134 */     return this.baseImponible;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBaseImponible(BigDecimal value)
/*     */   {
/* 146 */     this.baseImponible = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getValor()
/*     */   {
/* 158 */     return this.valor;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setValor(BigDecimal value)
/*     */   {
/* 170 */     this.valor = value;
/*     */   }
/*     */ }


/* Location:              F:\SRI001.war!\WEB-INF\classes\com\documentosElectronicos\notaCredito\Impuesto.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */