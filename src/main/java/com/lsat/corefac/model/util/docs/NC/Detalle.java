/*    */ package com.lsat.corefac.model.util.docs.NC;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="detalle", propOrder={"motivoModificacion"})
/*    */ public class Detalle
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String motivoModificacion;
/*    */   
/*    */   public String getMotivoModificacion()
/*    */   {
/* 47 */     return this.motivoModificacion;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setMotivoModificacion(String value)
/*    */   {
/* 59 */     this.motivoModificacion = value;
/*    */   }
/*    */ }


/* Location:              F:\SRI001.war!\WEB-INF\classes\com\documentosElectronicos\notaCredito\Detalle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */