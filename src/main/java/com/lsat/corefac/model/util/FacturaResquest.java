package com.lsat.corefac.model.util;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class FacturaResquest {
    private String api_key;

    private String version;
    private Long ccompany;
    private Long cdocument;
    private String codigoDoc;
    private Emisor emisor;
    private Comprador comprador;
    private List<Item> items;
    private List<Pago> pagos;
    private List<InformacionAdicional> informacionAdicional;


    @Getter
    @Setter
    public static class Pago {
        private String tipo;
        private String total;
    }

    @Getter
    @Setter
    public static class Item {
        private String codigo_principal;
        private String codigoauxiliar;
        private String descripcion;
        private int tipoproducto;
        private int tipo_iva;
        private BigDecimal precio_unitario;
        private int cantidad;
        private BigDecimal descuento;
        private int tipo_ice;
        private BigDecimal valor_ice;
        private int tarifa_ice;
    }

    @Getter
    @Setter
    public static class InformacionAdicional {
        private String nombre;
        private String detalle;
    }

    @Getter
    @Setter
    public static class Emisor {
        private String ambiente;
        private String tipoEmision;
        private String codDoc;
        private String establecimiento;
        private String ptoEmision;
        private String dirEstablecimiento;
        private String contribuyenteEspecial;
        private String obligadoLlevarContabilidad;
        private String ruc;
        private String razonSocial;

        private String nombreComercial;
        private String secuencial;
        private String fechaEmision;
    }

    @Getter
    @Setter
    public static class Comprador {
        private String tipoIdentificacion;
        private String identificacion;
        private String razonSocial;
        private String direccion;
        private String telefono;
        private String celular;
        private String correo;
    }

}
