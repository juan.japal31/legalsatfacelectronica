package com.lsat.corefac.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(name = "tcompany")
@Getter
@Setter
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long company;

    @Column(name = "name", length = 255)
    protected String name;

    @Column(name = "state")
    protected Character state;

    @Column(name = "date_authorization")
    protected String dateAuthorization;

    @Column(name = "direccion_establecimiento")
    protected String direccionEstablecimiento;

    @Column(name = "value")
    protected double value;


}
