package com.lsat.corefac.repository;

import com.lsat.corefac.model.Xml;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface XmlRepository extends PagingAndSortingRepository<Xml,Long> {



}
