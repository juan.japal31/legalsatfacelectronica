package com.lsat.corefac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreFacApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoreFacApplication.class, args);
    }

}
