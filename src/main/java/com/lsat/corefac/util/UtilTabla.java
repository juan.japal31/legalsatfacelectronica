package com.lsat.corefac.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilTabla {

    public static String nombreDocumento(String codDoc) {

        if ("01".equals(codDoc)) {
            return "FACTURA";
        }
        if ("04".equals(codDoc)) {
            return "NOTA DE CRÉDITO";
        }
        if ("05".equals(codDoc)) {
            return "NOTA DE DÉBITO";
        }
        if ("06".equals(codDoc)) {
            return "GUÍA REMISIÓN";
        }
        if ("07".equals(codDoc)) {
            return "COMPROBANTE DE RETENCIÓN";
        }
        return null;
    }

    public static String Table6(String value) {

        return "01";
    }

    public static String claveAcceso(String value) {

        return "022222222222222222222222222222222222222222221";
    }


}
