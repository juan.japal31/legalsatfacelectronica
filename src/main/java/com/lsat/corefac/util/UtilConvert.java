package com.lsat.corefac.util;

import com.lsat.corefac.model.util.docs.FE.Factura;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@UtilityClass
public class UtilConvert {
    public static final String SYSTEM_MAX_DATE = "31-12-2999 00:00:00";

    public static final String SYSTEM_MAX_DATE_ONLY_DATE = "31-12-2999";
    public static final String DATE_ONLY_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";

    public static Date convertStringToDate(final String fechaStr, final String pattern) throws ParseException {
        if (fechaStr == null || fechaStr.isEmpty()) {
            return null;
        }

        return (new SimpleDateFormat(pattern)).parse(fechaStr);
    }

    public static String convertDateToString(final Date date, final String pattern) {
        if (date == null) {
            return null;
        }
        return (new SimpleDateFormat(pattern)).format(date);
    }


    public static String jaxbObjectToXML(Object source,Class type)
    {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(type);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(source, sw);
            String xmlContent = sw.toString();
            return  xmlContent;
        } catch (JAXBException e) {
            log.error("error en metodo jaxbObjectToXML "+e);
            return  null;
        }
    }

}
