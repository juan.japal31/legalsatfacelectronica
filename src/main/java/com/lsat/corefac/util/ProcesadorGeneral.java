package com.lsat.corefac.util;

import com.lsat.corefac.model.Company;
import com.lsat.corefac.model.Document;
import com.lsat.corefac.model.Xml;
import com.lsat.corefac.model.util.ResponseGeneric;
import com.lsat.corefac.repository.DocumentRepository;
import com.lsat.corefac.repository.StepRepository;
import com.lsat.corefac.repository.XmlRepository;
import com.lsat.corefac.util.firma.Signer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

@Slf4j
public class ProcesadorGeneral {
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");


    private DocumentRepository documentRepository;

    private XmlRepository xmlRepository;

    private StepRepository stepRepository;




    private Document document;
    private Xml xml;
    private Company company;


    public void ProcesadorGeneral() {

    }

    public ProcesadorGeneral(Company company, Document document, Xml xml) {
        this.document = document;
        this.xml = xml;
        this.company = company;
    }

    public void execute() throws ExecutionException, InterruptedException {
        CompletableFuture<ResponseGeneric> future = CompletableFuture.supplyAsync(new Supplier<ResponseGeneric>() {
            @Override
            public ResponseGeneric get() {
                try {
                    if (!firmarDocumento()) {

                    }
                    return new ResponseGeneric(false, "", null);
                } catch (Exception e) {

                    return null;
                }
            }
        });

        ResponseGeneric result = future.get();
    }

    private boolean firmarDocumento() throws IOException {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.FIRMADO.name(), new Date().toString()));
        try {
            File tempXml = File.createTempFile(this.document.getKeyAcces(), ".xml");
            Files.writeString(tempXml.toPath(), "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> <factura id=\"comprobante\" version=\"1.0.0\"> <infoTributaria> <ambiente>1</ambiente> <tipoEmision>1</tipoEmision> <razonSocial>Jorge Luis</razonSocial> <nombreComercial>Ideas en Binario</nombreComercial> <ruc>1234567890001</ruc> <claveAcceso>0301202301123456789000110010030000000071234567811</claveAcceso> <codDoc>01</codDoc> <estab>001</estab> <ptoEmi>003</ptoEmi> <secuencial>000000007</secuencial> <dirMatriz>Ecuador</dirMatriz> </infoTributaria> <infoFactura> <fechaEmision>03/01/2023</fechaEmision> <dirEstablecimiento>Ecuador</dirEstablecimiento> <obligadoContabilidad>NO</obligadoContabilidad> <tipoIdentificacionComprador>07</tipoIdentificacionComprador> <razonSocialComprador>CONSUMIDOR FINAL</razonSocialComprador> <identificacionComprador>9999999999999</identificacionComprador> <totalSinImpuestos>10.00</totalSinImpuestos> <totalDescuento>0.00</totalDescuento> <totalConImpuestos> <totalImpuesto> <codigo>2</codigo> <codigoPorcentaje>2</codigoPorcentaje> <baseImponible>10.00</baseImponible> <tarifa>12.00</tarifa> <valor>1.20</valor> </totalImpuesto> </totalConImpuestos> <propina>0.00</propina> <importeTotal>11.20</importeTotal> <moneda>DOLAR</moneda> <pagos> <pago> <formaPago>01</formaPago> <total>11.20</total> </pago> </pagos> </infoFactura> <detalles> <detalle> <codigoPrincipal>3</codigoPrincipal> <descripcion>NORMAL CON IVA</descripcion> <cantidad>1</cantidad> <precioUnitario>10</precioUnitario> <descuento>0</descuento> <precioTotalSinImpuesto>10.00</precioTotalSinImpuesto> <impuestos> <impuesto> <codigo>2</codigo> <codigoPorcentaje>2</codigoPorcentaje> <tarifa>12.00</tarifa> <baseImponible>10.00</baseImponible> <valor>1.20</valor> </impuesto> </impuestos> </detalle> </detalles> </factura>", StandardCharsets.UTF_8, StandardOpenOption.APPEND);
          //  String xmlSing = signer.sign(document, "0101464402GMMT", temp.getPath(), "", "D:\\USERS\\ba0100064l\\Desktop\\archive\\0101464402001.p12");
            Signer signer = new Signer();
            File tempXmlSing = File.createTempFile(this.document.getKeyAcces()+"_Firmada_", ".xml");
//Mhoyos1234;NO
            signer.sign("D:\\USERS\\ba0100064l\\Desktop\\0919663062001.p12", "Mhoyos1234", tempXml.getPath(), tempXmlSing.getPath());



            this.xml.setXmlSigned("");
            this.xmlRepository.save(this.xml);
        } catch (Exception e) {
            log.error(String.format("Paso :%s, Error: %s", UtilEnum.PASO.FIRMADO.name(), e));
            return false;
        }
        return true;
    }

    private boolean generarRide() {

        return true;
    }

    private boolean enviarMail() {

        return true;
    }

    private boolean recepcion() {

        return true;
    }

    private boolean autorizacion() {

        return true;
    }


}
