package com.lsat.corefac.service;

import com.lsat.corefac.model.Company;
import com.lsat.corefac.model.Document;
import com.lsat.corefac.model.Xml;
import com.lsat.corefac.model.util.FacturaResquest;
import com.lsat.corefac.model.util.ResponseGeneric;
import com.lsat.corefac.model.util.docs.FE.Factura;
import com.lsat.corefac.model.util.docs.FE.InfoTributaria;
import com.lsat.corefac.model.util.docs.FE.ObligadoContabilidad;
import com.lsat.corefac.repository.CompanyRepository;
import com.lsat.corefac.repository.DocumentRepository;
import com.lsat.corefac.repository.XmlRepository;
import com.lsat.corefac.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.Optional;


@Slf4j
@Service
public class FacturaService {


    private final DocumentRepository documentRepository;
    private final CompanyRepository companyRepository;
    private final XmlRepository xmlRepository;

    @Autowired
    public FacturaService(DocumentRepository documentRepository, CompanyRepository companyRepository, XmlRepository xmlRepository) {
        this.documentRepository = documentRepository;
        this.companyRepository = companyRepository;
        this.xmlRepository = xmlRepository;
    }

    public ResponseGeneric emitirFactura(FacturaResquest facturaResquest) throws ParseException {

        Optional<Company> oneCompany = this.companyRepository.findById(facturaResquest.getCcompany());
        if (!oneCompany.isPresent()) {
            log.error("La compania no Existe SE PROCEDE A REGISTRAR");
            Company company = new Company();
            company.setCompany(facturaResquest.getCcompany());
            company.setName(facturaResquest.getEmisor().getRazonSocial());
            company.setState('A');
            oneCompany = Optional.of(companyRepository.save(company));
        }
        if (oneCompany.get().getState() != 'A') {
            log.error("La compania no esta activa");
            return new ResponseGeneric(true, "msg_empresa_no_existe", null);
        }

        Factura factura = new Factura();
        factura.setVersion(facturaResquest.getVersion());
        InfoTributaria infoTributaria = new InfoTributaria();
        infoTributaria.setAmbiente(facturaResquest.getEmisor().getAmbiente());
        infoTributaria.setTipoEmision(facturaResquest.getEmisor().getTipoEmision());
        infoTributaria.setRazonSocial(facturaResquest.getEmisor().getRazonSocial());
        infoTributaria.setNombreComercial(facturaResquest.getEmisor().getNombreComercial());
        infoTributaria.setRuc(facturaResquest.getEmisor().getRuc());
        infoTributaria.setClaveAcceso(ClaveDeAcceso.generaClave(UtilConvert.convertStringToDate(facturaResquest.getEmisor().getFechaEmision(),UtilConvert.DATE_ONLY_FORMAT),facturaResquest.getCodigoDoc(),facturaResquest.getEmisor().getRuc(),facturaResquest.getEmisor().getAmbiente(),(facturaResquest.getEmisor().getEstablecimiento()+facturaResquest.getEmisor().getPtoEmision()),facturaResquest.getEmisor().getSecuencial(),"12345678",facturaResquest.getEmisor().getTipoEmision()));
        infoTributaria.setCodDoc(facturaResquest.getEmisor().getCodDoc());
        infoTributaria.setEstab(facturaResquest.getEmisor().getEstablecimiento());
        infoTributaria.setPtoEmi(facturaResquest.getEmisor().getPtoEmision());
        infoTributaria.setSecuencial(facturaResquest.getEmisor().getSecuencial());
        infoTributaria.setDirMatriz(facturaResquest.getEmisor().getDirEstablecimiento());
        factura.setInfoTributaria(infoTributaria);
        ///////////////////////////////////////////////////////////////////

        Factura.Detalles detalles = new Factura.Detalles();
        for (FacturaResquest.Item item : facturaResquest.getItems()) {
            Factura.Detalles.Detalle oneDetalle = new Factura.Detalles.Detalle();
            oneDetalle.setCodigoPrincipal(item.getCodigo_principal());
            oneDetalle.setUnidadMedida(null);
            oneDetalle.setCantidad(new BigDecimal(item.getCantidad()));
            oneDetalle.setDescripcion(item.getDescripcion());
            oneDetalle.setCodigoAuxiliar(item.getCodigoauxiliar());
            oneDetalle.setPrecioUnitario(item.getPrecio_unitario());
            oneDetalle.setDescuento(item.getDescuento());
            detalles.getDetalle().add(oneDetalle);
        }


        Factura.InfoFactura infoFactura = new Factura.InfoFactura();
        infoFactura.setFechaEmision(facturaResquest.getEmisor().getFechaEmision());
        infoFactura.setDirEstablecimiento(facturaResquest.getEmisor().getDirEstablecimiento());
        infoFactura.setContribuyenteEspecial(facturaResquest.getEmisor().getContribuyenteEspecial());
        infoFactura.setObligadoContabilidad(facturaResquest.getEmisor().getObligadoLlevarContabilidad().equalsIgnoreCase("SI") ? ObligadoContabilidad.SI : ObligadoContabilidad.NO);
        infoFactura.setTipoIdentificacionComprador(UtilTabla.Table6(facturaResquest.getComprador().getTipoIdentificacion()));
        infoFactura.setGuiaRemision(null);
        infoFactura.setRazonSocialComprador(facturaResquest.getComprador().getRazonSocial());
        infoFactura.setIdentificacionComprador(facturaResquest.getComprador().getIdentificacion());
        infoFactura.setDireccionComprador(facturaResquest.getComprador().getDireccion());
        infoFactura.setTotalSinImpuestos(null);
        infoFactura.setTotalDescuento(null);
        factura.setInfoFactura(infoFactura);
        factura.setDetalles(detalles);
        /////////////////////////////////////////////////////////////////////////////////////////
        String xml = UtilConvert.jaxbObjectToXML(factura, Factura.class);
        if (xml == null) {
            log.error("Error al Generar el Xml");
            return new ResponseGeneric(true, "msg_error_generar_xml", null);
        }
        Optional<Document> oneDocumento = this.documentRepository.findById(facturaResquest.getCdocument());
        Optional<Xml> oneXml=null;
        if(!oneDocumento.isPresent()){
            Document document = new Document();
            document.setCcompany(oneCompany.get().getCompany());
            document.setAliasDoc(facturaResquest.getCodigoDoc());
            document.setDateRegistre(new Date());
            document.setName(UtilTabla.nombreDocumento(facturaResquest.getCodigoDoc()));
            document.setKeyAcces("62626256262626262626262615515151515151");
            document.setPaso(UtilEnum.PASO.REGISTRO);
            oneDocumento = Optional.of(this.documentRepository.save(document));
            Xml xml1 = new Xml();
            xml1.setXmlNormal(xml);
            xml1.setCdocument(document.getCdocument());
            oneXml= Optional.of(this.xmlRepository.save(xml1));
        }
        ProcesadorGeneral procesadorGeneral= new ProcesadorGeneral(oneCompany.get(),oneDocumento.get(),oneXml.get());
        try {
            procesadorGeneral.execute();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new ResponseGeneric<>(true, "OK", null);

    }


}
